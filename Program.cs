﻿using MQTTnet;
using MQTTnet.Client;  
using MQTTnet.Implementations;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;  
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Gpio;
using static System.Net.Mime.MediaTypeNames;

namespace MQTT.client
{
    class Program
    {
        static int i = 0;
        // Create a new MQTT client.
        static MqttFactory factory = new MqttFactory();
        static IMqttClient mqttClient = factory.CreateMqttClient();
        static TimeSpan span = new TimeSpan(0, 0, 0, 0, 100);       

        static void Main(string[] args)
        {  
            // Use TCP connection.
            var options = new MqttClientOptionsBuilder()
                .WithClientId("DOTNETCLIENT")
                .WithTcpServer("m23.cloudmqtt.com", 11472) // Port is optional
                .WithCredentials("zgbotdfy", "7GPCBT0VUAdf")
                .Build();

            // For .NET Framwork & netstandard apps:
            MqttTcpChannel.CustomCertificateValidationCallback = (x509Certificate, x509Chain, sslPolicyErrors, mqttClientTcpOptions) =>
            {
                if (mqttClientTcpOptions.Server == "server_with_revoked_cert")
                {
                    return true;
                }

                return false;
            };

            mqttClient.Disconnected += async (s, e) =>
            {
                Console.WriteLine("### DISCONNECTED FROM SERVER ###");
                await Task.Delay(TimeSpan.FromSeconds(5));

                try
                {
                    await mqttClient.ConnectAsync(options);
                }
                catch
                {
                    Console.WriteLine("### RECONNECTING FAILED ###");
                }
            };


            mqttClient.ConnectAsync(options);

            mqttClient.Connected += async (s, e) =>
            {
                Console.WriteLine("### CONNECTED WITH SERVER ###");

                // Subscribe to a topic
                await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic("GPIO_17").Build());
                await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic("GPIO_18").Build());

                Console.WriteLine("### SUBSCRIBED ###");
            };
                    
            mqttClient.ApplicationMessageReceived += async (s, e) =>
            {
                Console.WriteLine("### RECEIVED APPLICATION MESSAGE ###");
                Console.WriteLine($"+ Time = {DateTime.Now}");
                Console.WriteLine($"+ Topic = {e.ApplicationMessage.Topic}");
                Console.WriteLine($"+ Payload = {Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}");
                Console.WriteLine($"+ QoS = {e.ApplicationMessage.QualityOfServiceLevel}");
                Console.WriteLine($"+ Retain = {e.ApplicationMessage.Retain}");
                Console.WriteLine();

                string GPIO = e.ApplicationMessage.Topic;
                string status = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);

                var message = new MqttApplicationMessageBuilder()
               .WithTopic("StatusRPI")
               .WithPayload(GPIO + " " + status)
               .WithExactlyOnceQoS()
               .WithRetainFlag()
               .Build();

                await mqttClient.PublishAsync(message);

                ChangeGPIO(GPIO, status);
            };

            Task task = PeriodicSendInfo(span);         

            Console.ReadKey();
           
        }

        public static void ChangeGPIO(string GPIO, string status)
        {
            int GPIOnumber = Convert.ToInt32(GPIO.Remove(0,5));
            bool isOn;
            if(status == "On")
            {
                isOn = true;
            }
            else
            {
                isOn = false;
            }
            // Get a reference to the pin you need to use.
            // All 3 methods below are exactly equivalent

            var wiringPiNumber = SearchNumber(GPIOnumber);

            var blinkingPin = Pi.Gpio[wiringPiNumber];  

            // Configure the pin as an output
            blinkingPin.PinMode = GpioPinDriveMode.Output;            
            blinkingPin.Write(isOn);
            System.Threading.Thread.Sleep(500);
        }

        public static int SearchNumber(int GPIOnumber)
        {
            switch (GPIOnumber)
            {
                case 17:
                    return 0;
                case 18:
                    return 1;
                default:
                    return 0;
            }
        }

        public static int SearchGPIONumber(int wiringPInumber)
        {
            switch (wiringPInumber)
            {
                case 0:
                    return 17;
                case 1:
                    return 18;
                default:
                    return 0;
            }
        }

        public static string CheckStatusGPIO(int wiringPInumber)
        {
            var Pin = Pi.Gpio[wiringPInumber];
            if (Pin.Read())
            {
                return "On";
            }
            else
            {
                return "Off";
            }
        }

        public static async Task PeriodicSendInfo(TimeSpan interval)
        {
            while (true)
            {
                await SendInfoRPI();
                await Task.Delay(interval);
            }
        }    

        static async Task SendInfoRPI()
        {
            Console.WriteLine("GPIO " + i);
            int GPIO = SeachGPIONumber(i);
            string status = CheckStatusGPIO(i);

            // Register a device on the bus
            var myDevice = Pi.I2C.AddDevice(0x48);

            var response = myDevice.Read().ToString();
            int temperature = Convert.ToInt32(response);  

            var message = new MqttApplicationMessageBuilder()
                .WithTopic("InfoRPI")
                .WithPayload(GPIO + " " + status + ";temperature: " + temperature)
                .WithExactlyOnceQoS()
                .WithRetainFlag()
                .Build();

            await mqttClient.PublishAsync(message);

            i = i + 1;
            
            if (i > 31)
            {
                i = 0;
            }
        } 
    }
}
